﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft;

namespace CheckersGame
{
    class PanelBoard
    {
        public bool m_IsSoldierXTurn = true;
        private FormDamka m_ParentForm;
        private Pieces[,] m_CheckersBoardPieces;
        private int m_GridSize;
        private Color m_SelectedSoldierColor;
        private Color m_NotSelectedSoldierColor;
        private static int m_NumOfWinX = 0;
        private static int m_NumOfWinO = 0;

        public int NumOfWinX
        {
            get { return m_NumOfWinX; }
            set { m_NumOfWinX = value; }
        }
        public int NumOfWinO
        {
            get { return m_NumOfWinO; }
            set { m_NumOfWinO = value; }
        }


        public PanelBoard(int GridSize, FormDamka parentForm)
        {
            this.m_ParentForm = parentForm;
            this.m_SelectedSoldierColor = Color.Blue;
            this.m_NotSelectedSoldierColor = Color.White;
            this.m_GridSize = GridSize;
            this.m_CheckersBoardPieces = new Pieces[this.m_GridSize, this.m_GridSize];
        }
        public void AddPiece(Pieces newPiece, int row, int col)
        {
            this.m_CheckersBoardPieces[row, col] = newPiece;
        }

        private void TogglePlayerTurn()
        {
            this.ResetJustAte();
            this.m_IsSoldierXTurn = !this.m_IsSoldierXTurn;

            m_ParentForm.ChangeTextLabelTurn();

            if (this.m_IsSoldierXTurn == false && this.m_ParentForm.m_ParentSettings.isMultiplePlayers == false)
            {
                this.ComputerMakeMove();
            }
        }

        private void ResetJustAte()
        {
            for (int row = 0; row < m_GridSize; row++)
            {
                for (int col = 0; col < m_GridSize; col++)
                {
                    if (this.m_CheckersBoardPieces[row, col].m_Type != ePieceType.Area)
                    {
                        this.m_CheckersBoardPieces[row, col].m_JustAte = false;
                    }
                }
            }
        }

        public Pieces GetPieceAtPosition(int row, int col)
        {
            try
            {
                return this.m_CheckersBoardPieces[row, col];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void ToggleSelectSoldier(Pieces piece, bool shouldSelect)
        {
            if (piece is null == false)
            {
                if (piece.m_Type != ePieceType.Area)
                {
                    piece.m_IsSelected = shouldSelect;
                    piece.m_Soldier.BackColor = piece.m_IsSelected ? this.m_SelectedSoldierColor : this.m_NotSelectedSoldierColor;
                }
            }
        }

        public void UnSelectAllSoldiersExcept(Pieces piece)
        {
            this.UnSelectAllSoldiers();
            this.ToggleSelectSoldier(this.m_CheckersBoardPieces[piece.Row, piece.Col], true);
        }

        public Pieces GetSelectedSoldier()
        {
            Pieces selected = null;
            for (int row = 0; row < m_GridSize; row++)
            {
                for (int col = 0; col < m_GridSize; col++)
                {
                    if (this.m_CheckersBoardPieces[row, col].m_IsSelected)
                    {
                        selected = this.m_CheckersBoardPieces[row, col];
                    }
                }
            }
            return selected;
        }

        public void UnSelectAllSoldiers()
        {
            for (int row = 0; row < m_GridSize; row++)
            {
                for (int col = 0; col < m_GridSize; col++)
                {
                    this.ToggleSelectSoldier(this.m_CheckersBoardPieces[row, col], false);
                }
            }
        }

        private void ComputerMakeMove()
        {
            bool done = false;
            foreach (Pieces soldier in this.m_CheckersBoardPieces)
            {
                if (!done)
                {
                    if (soldier.m_Type == ePieceType.O_Soldier) // the computer is the seconde player = O_soldier
                    {
                        foreach (Pieces toArea in this.m_CheckersBoardPieces)
                        {
                            if (!done)
                            {
                                if (toArea.m_Type == ePieceType.Area) // the computer is the seconde player = O_soldier
                                {
                                    if (AttemptMove(soldier, toArea, true))
                                    {
                                        done = true;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public bool AttemptMove(Pieces soldier, Pieces toArea, bool withoutMessageBoxes = false)
        {
            bool successMove = false;
            // if i can go to the current clicked position
            if (this.ValidMovePosition(soldier, toArea))
            {
                // update the selected button x,y to be the current x,y + update the board pieces array
                this.SwitchPosition(soldier, toArea);
                successMove = true;
            }
            else
            {
                Pieces eatenSoldier = this.ValidMoveWithEating(soldier, toArea);
                if (eatenSoldier is null == false)
                {
                    // replace the eaten with new piece.area panel
                    this.EatSoldier(eatenSoldier);
                    soldier.m_JustAte = true;

                    // move the soldier
                    this.SwitchPosition(soldier, toArea);
                    successMove = true;
                    CheckIfVictory();
                }
                else
                {
                    if (!withoutMessageBoxes)
                    {
                        MessageBox.Show("You can't do this move");
                    }
                }
            }
            this.UnSelectAllSoldiers();
            return successMove;
        }

        private void CheckIfVictory()
        {
            int countsoldierX = 0;
            int countsoldierO = 0;
            foreach (Pieces item in m_CheckersBoardPieces)
            {
                if (item.m_Type == ePieceType.X_Soldier)
                {
                    countsoldierX++;
                }
                if (item.m_Type == ePieceType.O_Soldier)
                {
                    countsoldierO++;
                }
            }
            if (countsoldierX == 0)
            {
                m_NumOfWinO++;
                PromptCheckAnotherRound(2);
            }
            if (countsoldierO == 0)
            {
                m_NumOfWinX++;
                PromptCheckAnotherRound(1);
            }
            if ((countsoldierX == 0 && countsoldierO == 0))
            {
                PromptCheckAnotherRound(3);
            }
        }

        private void PromptCheckAnotherRound(int whowin)
        {
            String question = String.Format("{0}!\nAnother round?", whowin == 3 ? "Tie" : "Player" + whowin + " Won");
            if (MessageBox.Show(question, "Damka", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                m_ParentForm.Hide();
                m_ParentForm.UpdateWinsLabels(m_NumOfWinX, m_NumOfWinO);
                m_ParentForm.m_ParentSettings.StartGame(m_NumOfWinX, m_NumOfWinO);
            }
            else
            {
                m_ParentForm.Close();
            }
        }

        private void EatSoldier(Pieces eatenSoldier)
        {
            int tempCol = eatenSoldier.Col;
            int tempRow = eatenSoldier.Row;
            Point tempLocation = ((Button)eatenSoldier.m_Soldier).Location;
            this.m_ParentForm.Controls.Remove(((Button)eatenSoldier.m_Soldier));
            eatenSoldier = null;

            Pieces newPiece = new Pieces(ePieceType.Area, tempCol, tempRow, Color.White, this);
            this.m_ParentForm.Controls.Add(newPiece.m_Area);
            this.AddPiece(newPiece, tempRow, tempCol);
        }

        private void SwitchPosition(Pieces soldier, Pieces toArea)
        {
            if (soldier.m_Type != ePieceType.Area && toArea.m_Type == ePieceType.Area)
            {
                int tempCol = toArea.Col;
                int tempRow = toArea.Row;
                Point tempLocation = ((Panel)toArea.m_Area).Location;

                toArea.m_Area.Location = soldier.m_Soldier.Location;
                toArea.Col = soldier.Col;
                toArea.Row = soldier.Row;

                soldier.m_Soldier.Location = tempLocation;
                soldier.Col = tempCol;
                soldier.Row = tempRow;

                this.m_CheckersBoardPieces[toArea.Row, toArea.Col] = toArea;
                this.m_CheckersBoardPieces[soldier.Row, soldier.Col] = soldier;

                this.CheckIfNeedToUpgradeToKing(soldier);

                if (soldier.m_JustAte && soldier.CanEatMore())
                {
                    // do nothing
                    soldier.m_JustAte = false;
                }
                else
                {
                    this.TogglePlayerTurn();
                }
            }
        }

        private void CheckIfNeedToUpgradeToKing(Pieces soldier)
        {
            if (soldier.m_Type == ePieceType.O_Soldier)
            {
                if (soldier.Row == 0)
                {
                    soldier.UpgradeToKing();
                }
            }
            else if (soldier.m_Type == ePieceType.X_Soldier)
            {
                if (soldier.Row == m_GridSize - 1)
                {
                    soldier.UpgradeToKing();
                }
            }
        }

        private Pieces EatOpponentInBetween(Pieces soldier, Pieces toArea)
        {
            int colToCheck, rowToCheck;
            Pieces pieceToCheck = null;
            Pieces eatenOppenent = null;
            int colDiff = soldier.Col - toArea.Col;
            int rowDiff = soldier.Row - toArea.Row;

            colToCheck = (colDiff > 0) ? soldier.Col - 1 : soldier.Col + 1;
            if (soldier.m_IsKing)
            {
                rowToCheck = rowDiff < 0 ? soldier.Row + 1 : soldier.Row - 1;
            }
            else
            {
                rowToCheck = soldier.m_Type == ePieceType.X_Soldier ? soldier.Row + 1 : soldier.Row - 1;
            }
            pieceToCheck = this.m_CheckersBoardPieces[rowToCheck, colToCheck];

            // check if any foe in between
            if (pieceToCheck.m_Type != soldier.m_Type && pieceToCheck.m_Type != ePieceType.Area)
            {
                eatenOppenent = pieceToCheck;
            }
            return eatenOppenent;
        }

        private Pieces ValidMoveWithEating(Pieces soldier, Pieces toArea)
        {
            Pieces eatenOpponent = null;
            int colDiff = Math.Abs(soldier.Col - toArea.Col);
            int rowDiff = Math.Abs(soldier.Row - toArea.Row);
            if (soldier.IsMoveLegitimate(toArea) && (colDiff + rowDiff == 4))
            {
                eatenOpponent = this.EatOpponentInBetween(soldier, toArea);
            }
            return eatenOpponent;
        }

        private bool ValidMovePosition(Pieces soldier, Pieces toArea)
        {
            bool isValid = true;
            if (soldier.IsMoveLegitimate(toArea) == false)
            {
                isValid = false;
            }

            if (soldier.IsMoveTooFar(toArea))
            {
                isValid = false;
            }
            else
            {
                if (!soldier.m_IsKing)
                {
                    if (soldier.IsMoveBackward(toArea))
                    {
                        isValid = false;
                    }
                }
                // if im a king and not too far then all is good
            }

            return isValid;
        }
    }
}
