﻿using System.Windows.Forms;

namespace CheckersGame
{
    partial class FormDamka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDamka));
            this.label1 = new System.Windows.Forms.Label();
            this.label_player1_wins_counter = new System.Windows.Forms.Label();
            this.label_player2_wins_counter = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel_scores = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.labelPlayerTurn = new System.Windows.Forms.Label();
            this.panel_scores.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label_player1_wins_counter
            // 
            resources.ApplyResources(this.label_player1_wins_counter, "label_player1_wins_counter");
            this.label_player1_wins_counter.Name = "label_player1_wins_counter";
            // 
            // label_player2_wins_counter
            // 
            resources.ApplyResources(this.label_player2_wins_counter, "label_player2_wins_counter");
            this.label_player2_wins_counter.Name = "label_player2_wins_counter";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // panel_scores
            // 
            resources.ApplyResources(this.panel_scores, "panel_scores");
            this.panel_scores.Controls.Add(this.label4);
            this.panel_scores.Controls.Add(this.label_player2_wins_counter);
            this.panel_scores.Controls.Add(this.label1);
            this.panel_scores.Controls.Add(this.label_player1_wins_counter);
            this.panel_scores.Name = "panel_scores";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // labelPlayerTurn
            // 
            resources.ApplyResources(this.labelPlayerTurn, "labelPlayerTurn");
            this.labelPlayerTurn.Name = "labelPlayerTurn";
            // 
            // FormDamka
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelPlayerTurn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel_scores);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "FormDamka";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.panel_scores.ResumeLayout(false);
            this.panel_scores.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_player1_wins_counter;
        private System.Windows.Forms.Label label_player2_wins_counter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel_scores;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelPlayerTurn;
    }
}