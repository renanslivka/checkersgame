﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckersGame
{
    public partial class FormDamka : Form
    {
        private int m_GridSize;
        private int m_TileSize;
        private Color m_Color1;
        private Color m_Color2;
        private PanelBoard m_Board;
        public FormGameSetting m_ParentSettings;


        public FormDamka(int size, FormGameSetting parentSettings)
        {
            this.m_ParentSettings = parentSettings;
            this.m_GridSize = size;
            this.m_TileSize = 60;
            this.Width = this.m_TileSize * size;
            this.Height = 60 + this.m_TileSize * size;
            this.m_Color1 = Color.DarkGray;
            this.m_Color2 = Color.White;
            this.m_Board = new PanelBoard(m_GridSize, this);
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

            // initialize the "checker board"
            //m_CheckersBoardButtons = new Pieces[this.m_GridSize, this.m_GridSize];

            // double for loop to handle all rows and columns
            for (int row = 0, i = 0; row < m_GridSize; row++, i++)
            {
                for (int col = 0; col < m_GridSize; col++)
                {
                    ePieceType newPieceType = ePieceType.Area;
                    Color newPeiceColor = m_Color2;

                    if (col % 2 == 0)
                    {
                        newPeiceColor = row % 2 != 1 ? m_Color1 : m_Color2;
                    }
                    else
                    {
                        newPeiceColor = row % 2 != 1 ? m_Color2 : m_Color1;
                    }

                    if ((row == i) || (row > m_GridSize - i))
                    {
                        if (i < m_GridSize / 2 - 1)
                        {
                            if ((row % 2 == 0) && (col % 2 == 1))
                            {
                                newPieceType = ePieceType.X_Soldier;
                            }
                            if (row % 2 == 1 && col % 2 == 0)
                            {
                                newPieceType = ePieceType.X_Soldier;
                            }
                        }
                        else
                        {
                            if (row > m_GridSize - i)
                            {
                                if ((row % 2 == 0) && (col % 2 == 1))
                                {
                                    newPieceType = ePieceType.O_Soldier;
                                }
                                if (row % 2 == 1 && col % 2 == 0)
                                {
                                    newPieceType = ePieceType.O_Soldier;
                                }
                            }
                        }
                    }
                    Pieces newPiece = new Pieces(newPieceType, col, row, newPeiceColor, this.m_Board);
                    if (newPieceType == ePieceType.Area)
                    {
                        Controls.Add(newPiece.m_Area);
                    }
                    else
                    {
                        Controls.Add(newPiece.m_Soldier);
                    }
                    this.m_Board.AddPiece(newPiece, row, col);
                }
            }
            //this.labelPlayerTurn.Text = this.board.m_IsSoldierXTurn == true ? labelPlayerTurn.Text = "Player1" : labelPlayerTurn.Text = "Player2"; 
            if (this.m_Board.m_IsSoldierXTurn)
            {
                labelPlayerTurn.Text = "Player1";
            }
            else
            {
                labelPlayerTurn.Text = "Player2";
            }
        }

        public void ChangeTextLabelTurn()
        {
            this.labelPlayerTurn.Text = m_Board.m_IsSoldierXTurn ? "player1" : "player2";
        }

        public void UpdateWinsLabels(int cX, int cO)
        {
            this.label_player1_wins_counter.Text = cX.ToString();
            this.label_player2_wins_counter.Text = cO.ToString();
        }
    }
}
