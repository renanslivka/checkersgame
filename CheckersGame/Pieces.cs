﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckersGame
{
    class Pieces
    {
        public Button m_Soldier;
        public Panel m_Area;
        private int m_TileSize = 60;
        public bool m_IsSelected = false;
        private PanelBoard m_ParentBoard;
        private int m_Row;
        private int m_Col;
        public bool m_IsKing;
        public ePieceType m_Type;
        public bool m_JustAte = false;

        public Pieces(ePieceType type, int col, int row, Color color, PanelBoard parent)
        {
            this.m_ParentBoard = parent;
            this.m_Row = row;
            this.m_Col = col;
            this.m_IsKing = false;
            this.m_Type = type;
            if (type == ePieceType.Area)
            {
                this.m_Area = new Panel
                {
                    Size = new Size(this.m_TileSize, this.m_TileSize),
                    Location = new Point(this.m_TileSize * col, this.m_TileSize * (row + 1))
                };
                this.m_Area.BackColor = color;
                this.m_Area.BorderStyle = BorderStyle.FixedSingle;
                this.m_Area.Click += new System.EventHandler(area_Clicked);
            }
            else
            {
                CreatSoldier((m_Type == ePieceType.X_Soldier ? "X" : "O"), col, row, color);
            }
        }

        public void UpgradeToKing()
        {
            if (!this.m_IsKing)
            {
                this.m_IsKing = true;
                this.m_Soldier.Text += "_K";
            }
        }

        private void CreatSoldier(string text, int col, int row, Color color)
        {
            this.m_Soldier = new Button
            {
                Size = new Size(this.m_TileSize, this.m_TileSize),
                Location = new Point(this.m_TileSize * col, this.m_TileSize * (row + 1))
            };
            this.m_Soldier.Text = text;
            this.m_Soldier.BackColor = color;
            this.m_Soldier.Click += new System.EventHandler(button_Clicked);

        }

        private bool IsMyTurn()
        {
            return (this.m_ParentBoard.m_IsSoldierXTurn && this.m_Type == ePieceType.X_Soldier) || (!this.m_ParentBoard.m_IsSoldierXTurn && this.m_Type == ePieceType.O_Soldier);
        }

        private void button_Clicked(object sender, EventArgs e)
        {
            if (this.IsMyTurn())
            {
                if (this.m_IsSelected)
                {
                    // this will unselect all
                    this.m_ParentBoard.UnSelectAllSoldiers();
                }
                else
                {
                    this.m_ParentBoard.UnSelectAllSoldiersExcept(this);
                }
            }
        }

        private void area_Clicked(object sender, EventArgs e)
        {
            Panel currentArea = (Panel)sender;
            if (currentArea.BackColor == Color.White)
            {
                // if there is any clicked button on the board
                Pieces selectedSoldier = this.m_ParentBoard.GetSelectedSoldier();
                if (selectedSoldier is null == false)
                {
                    this.m_ParentBoard.AttemptMove(selectedSoldier, this);
                }
            }

        }
        public int Row
        {
            get { return m_Row; }
            set { m_Row = value; }
        }

        public int Col
        {
            get { return m_Col; }
            set { m_Col = value; }
        }
        public bool IsMoveLegitimate(Pieces toArea)
        {
            int colDiff = Math.Abs(this.Col - toArea.Col);
            int rowDiff = Math.Abs(this.Row - toArea.Row);
            return (colDiff == 1 && rowDiff == 1) || (colDiff == 2 && rowDiff == 2);
        }

        public bool IsMoveTooFar(Pieces toArea)
        {
            int colDiff = Math.Abs(this.Col - toArea.Col);
            int rowDiff = Math.Abs(this.Row - toArea.Row);
            // Debug.WriteLine("**** colDiff: {0} , rowDiff: {1}", colDiff, rowDiff);
            return colDiff + rowDiff != 2;
        }

        public bool IsMoveBackward(Pieces toArea)
        {
            int colDiff = Math.Abs(this.Col - toArea.Col);
            int rowDiff = this.Row - toArea.Row;
            bool isValid = false;
            if (this.m_Type == ePieceType.X_Soldier)
            {
                isValid = colDiff + rowDiff == 2;
            }
            else if (this.m_Type == ePieceType.O_Soldier)
            {
                isValid = colDiff + rowDiff == 0;
            }
            return isValid;
        }

        public bool CanEatMore()
        {
            bool foodInMyRange = false;
            Pieces nextLeftPiece = this.m_ParentBoard.GetPieceAtPosition(this.m_Row + 1, this.m_Col + 1);
            Pieces nextRigthPiece = this.m_ParentBoard.GetPieceAtPosition(this.m_Row + 1, this.m_Col - 1);
            Pieces nextNextLeftPiece = this.m_ParentBoard.GetPieceAtPosition(this.m_Row + 2, this.m_Col + 2);
            Pieces nextNextRigthPiece = this.m_ParentBoard.GetPieceAtPosition(this.m_Row + 2, this.m_Col - 2);

            Pieces prevLeftPiece = this.m_ParentBoard.GetPieceAtPosition(this.m_Row - 1, this.m_Col + 1);
            Pieces prevRigthPiece = this.m_ParentBoard.GetPieceAtPosition(this.m_Row - 1, this.m_Col - 1);
            Pieces prevPrevLeftPiece = this.m_ParentBoard.GetPieceAtPosition(this.m_Row - 2, this.m_Col + 2);
            Pieces prevPrevRigthPiece = this.m_ParentBoard.GetPieceAtPosition(this.m_Row - 2, this.m_Col - 2);

            if (this.m_IsKing)
            {
                if (this.m_Type == ePieceType.X_Soldier)
                {
                    if (nextLeftPiece != null && nextNextLeftPiece != null && nextLeftPiece.m_Type == ePieceType.O_Soldier && nextNextLeftPiece.m_Type == ePieceType.Area)
                    {
                        foodInMyRange = true;
                    }
                    else if (nextRigthPiece != null && nextNextRigthPiece != null && nextRigthPiece.m_Type == ePieceType.O_Soldier && nextNextRigthPiece.m_Type == ePieceType.Area)
                    {
                        foodInMyRange = true;
                    }
                }
                else
                {
                    if (this.m_Type == ePieceType.O_Soldier)
                    {
                        if (prevLeftPiece != null && prevPrevLeftPiece != null && prevLeftPiece.m_Type == ePieceType.X_Soldier && prevPrevLeftPiece.m_Type == ePieceType.Area)
                        {
                            foodInMyRange = true;
                        }
                        else if (prevRigthPiece != null && prevPrevRigthPiece != null && prevRigthPiece.m_Type == ePieceType.X_Soldier && prevPrevRigthPiece.m_Type == ePieceType.Area)
                        {
                            foodInMyRange = true;
                        }
                    }
                }
            }
            else if (this.m_Type == ePieceType.X_Soldier)
            {
                if (nextLeftPiece != null && nextNextLeftPiece != null && nextLeftPiece.m_Type == ePieceType.O_Soldier && nextNextLeftPiece.m_Type == ePieceType.Area)
                {
                    foodInMyRange = true;
                }
                else if (nextRigthPiece != null && nextNextRigthPiece != null && nextRigthPiece.m_Type == ePieceType.O_Soldier && nextNextRigthPiece.m_Type == ePieceType.Area)
                {
                    foodInMyRange = true;
                }

            }
            else if (this.m_Type == ePieceType.O_Soldier)
            {
                if (prevLeftPiece != null && prevPrevLeftPiece != null && prevLeftPiece.m_Type == ePieceType.X_Soldier && prevPrevLeftPiece.m_Type == ePieceType.Area)
                {
                    foodInMyRange = true;
                }
                else if (prevRigthPiece != null && prevPrevRigthPiece != null && prevRigthPiece.m_Type == ePieceType.X_Soldier && prevPrevRigthPiece.m_Type == ePieceType.Area)
                {
                    foodInMyRange = true;
                }
            }
            return foodInMyRange;
        }
    }

    enum ePieceType
    {
        Area = 0,
        X_Soldier = 1,
        O_Soldier = 2
    }
}
