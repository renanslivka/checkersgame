﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CheckersGame
{
    public partial class FormGameSetting : Form
    {
        public FormDamka f2;
        #region constractor
        public FormGameSetting()
        {
            InitializeComponent();
        }
        #endregion
        #region Methods
        // the size of the board
        public int board_size;
        public bool isMultiplePlayers = false;
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            this.board_size = 6;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            this.board_size = 8;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            this.board_size = 10;
        }
        //End the size of the board

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            IfButtonIsPressedThenCanWriteInTextbox();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxNamePlayer1.Text == string.Empty)
            {
                MessageBox.Show("Please write the name of player 1");
            }
            else
            {
                StartGame(0, 0);
            }

        }

        public void StartGame(int player1WinsCounter, int player2WinsCounter)
        {
            this.Hide();
            this.isMultiplePlayers = this.checkBoxNamePlayer2.Checked;
            f2 = new FormDamka(this.board_size, this);
            f2.UpdateWinsLabels(player1WinsCounter, player2WinsCounter);
            f2.ShowDialog();
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            IfButtonIsPressedThenCanWriteInTextbox();
        }

        #endregion

        private void IfButtonIsPressedThenCanWriteInTextbox()
        {
            if (this.checkBoxNamePlayer2.Checked == true)
            {
                this.textBoxNamePlayer2.Enabled = true;
                this.textBoxNamePlayer2.Focus();
            }
            else
            {
                this.textBoxNamePlayer2.Enabled = false;
            }
        }

        private void labelBoardSize_Click(object sender, EventArgs e)
        {

        }
    }
}